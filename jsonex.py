#json example
import json

# difference between array and list in python
# Array is a sequential data type, data can be of same data type
# list is a sequential data type, stores data of multiple data types

# Java Script Object Notation
# It is a key-value pair object

# XML  VS JSON

# Read from a json file
fd = open("example.json", 'r')
jsonobj = json.load(fd)
fd.close()

print(type(jsonobj))
print(jsonobj[0])
print(jsonobj[0]['timings'])
if 'timings' in jsonobj[1]:
    print(jsonobj[1]['timings'])
else:
    print(jsonobj[1])

# convert dictionary into json object
mydict = {
    'name': 'quantom'
}
print(type(mydict))
jsonstr = json.dumps(mydict)
print(type(jsonstr))
print(jsonstr)

# convert to json object from a string
jobj = json.loads(jsonstr)
print(type(jobj))
print(jobj['name'])


# Write a json file
fd = open("new.json", 'w')
json.dump(mydict, fd)
fd.close()

