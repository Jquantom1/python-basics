
# lambda is anonymous (adhoc)function

# lambda arguments : expression

addnum = lambda a, b: a + b
val = addnum(2, 3)
print(val)

sqr = lambda a : a * a
val = sqr(9)
print(val)
